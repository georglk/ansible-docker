# ANSIBLE-DOCKER

Docker image for Ansible.

## Build local image

```shell
docker builder prune -f
docker rmi -f ansible & docker build . -f Dockerfile -t ansible
docker inspect -f "{{ .Size }}" ansible
```

## Executing commands

### Executing ansible-playbook

Usage `ansible-playbook`

```shell
docker run --rm -i -t --name ansible ansible ansible-playbook <optional arguments> <playbook file> [<playbook file> ...]
``` 

Get `--help`

```shell
docker run --rm -i -t --name ansible ansible ansible-playbook --help
```

Авторизация:

```shell
-u REMOTE_USER
-k, --ask-pass
--private-key PRIVATE_KEY
```

Опции соединения:

```shell
--ssh-extra-args SSH_EXTRA_ARGS
```

Повышение привилегий:

```shell
-b, --become
--become-user BECOME_USER
-K, --ask-become-pass
```

Проверка синтаксиса:

```shell
--syntax-check
--list-hosts
```

Работа с тегами:

```shell
--list-tasks
-t TAGS, --tags TAGS
--skip-tags SKIP_TAGS
```

### Lint ansible-playbook

Usage `yamllint`

```shell
docker run --rm -i -t --name ansible ansible yamllint <optional arguments> <playbook file> [<playbook file> ...]
```

Usage `ansible-lint`

```shell
docker run --rm -i -t --name ansible ansible ansible-lint <optional arguments> <playbook file> [<playbook file> ...]
```
