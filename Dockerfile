FROM ubuntu:20.04

RUN DEBIAN_FRONTEND=noninteractive \
apt-get update && \
apt-get install --no-install-recommends -y ansible && \
apt-get install --no-install-recommends -y ansible-lint yamllint && \
rm -rf /var/lib/apt/lists/*

RUN ansible-galaxy collection install community.general
RUN ansible-galaxy install newrelic.newrelic-infra

ADD entrypoint.sh /entrypoint.sh
RUN chmod 644 /entrypoint.sh && chmod u+x,g+x /entrypoint.sh

ENTRYPOINT ["/entrypoint.sh"]

# default command
CMD [ "ansible", "--version" ]

# https://docs.docker.com/engine/reference/builder
