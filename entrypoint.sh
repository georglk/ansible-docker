#!/bin/bash

if \
  [ "$1" = "ansible" ] || \
  [ "$1" = "ansible-config" ] || \
  [ "$1" = "ansible-console" ] || \
  [ "$1" = "ansible-doc" ] || \
  [ "$1" = "ansible-galaxy" ] || \
  [ "$1" = "ansible-inventory" ] || \
  [ "$1" = "ansible-playbook" ] || \
  [ "$1" = "ansible-pull" ] || \
  [ "$1" = "ansible-vault" ] || \
  [ "$1" = "yamllint" ] || \
  [ "$1" = "ansible-lint" ]
then
  command="$1"
  shift
  exec "$command" "$@"
elif [ "$1" = "exec" ]
then
  shift
  exec "$@"
elif [ "$1" = "shell" ]
then
  shift
  "/bin/bash"
else
  echo "unrecognized parameter: "$1""
  exit 1
fi
